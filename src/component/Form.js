import React, { useState } from "react";
import Input from "./Input";
import Button from "./Button";

function Form(props) {
  const { onAdd } = props;
  const [inputValue, setInputValue] = useState("");
  const onChange = (event) => {
    setInputValue(event.target.value);
  };
  const onClick = (event) => {
    event.preventDefault();
    onAdd(inputValue);
    setInputValue("");
  };
  return (
    <form>
      <Input value={inputValue} onChange={onChange} type="text" />
      <Button type="submit" onClick={onClick} text="add" />
    </form>
  );
}
export default Form;
