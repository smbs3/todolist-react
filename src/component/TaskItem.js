import React, { useState } from "react";
import Input from "./Input";
import Button from "./Button";

export const TaskItem = (props) => {
  const { item, onDelete, toggleComplete, list, setList } = props;
  const [isEditing, setisEditing] = useState(false);
  const [editingText, setEditingText] = useState("");


  const handleEdit = () => {
    setisEditing(true);
    setEditingText(item.text);
  };

  const handleCheck = () => {
    toggleComplete(item.id);
  };
  const handleDelete = () => {
    onDelete(item.id);
  };
  const handleEditSubmit = () => {
    setisEditing(false);
    const updateList = [...list];
    const index = updateList.findIndex((task) => item.id === task.id);
    const itemUpdate = updateList[index];
    updateList.splice(index, 1, { ...itemUpdate, text: editingText });
    setList(updateList);
    localStorage.setItem("todos", JSON.stringify(updateList));
  };
  const onChange = (event) => {
    const text = event.target.value;
    setEditingText(text);
  };
  const renderContent = () => {
    if (isEditing)
      return (
        <>
          <Input type="text" onChange={onChange} value={editingText} />
          <Button type="button" onClick={handleEditSubmit} text="save" />
        </>
      );
    return (
      <>
        <span>{item.text}</span>
        <Button type="button" onClick={handleEdit} text="Edit" />
      </>
    );
  };

  return (
    <div key={item.id}>
      <Input type="checkbox" onChange={handleCheck} checked={item.complete} />
      {renderContent()}
      <Button type="button" onClick={handleDelete} text="Delete" />
    </div>
  );
};
