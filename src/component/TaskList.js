import React from 'react'
import { TaskItem } from './TaskItem';

export const TaskList = (props) => {
  const {list, onDelete, toggleComplete, setList}= props;

  const renderItems = ()=> {
    return list?.map(item=> <TaskItem item={item} key={item.id} onDelete={onDelete}  toggleComplete={toggleComplete} list={list} setList={setList} />)
  }


  return (
    <ul>
      {renderItems()}
    </ul>
  )
}
