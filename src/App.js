import React, { useState, useEffect } from "react";
import "./App.css";
import Form from "./component/Form";
import { TaskList } from "./component/TaskList";

function App() {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    const list = JSON.parse(localStorage.getItem("todos"));
    if (list) {
      setTodos(list);
    }
  }, []);

  const onAdd = (value) => {
    const newValue = {
      id: new Date().getTime(),
      text: value,
      complete: false,
    };
    const list = [...todos, newValue];
    setTodos(list)
    localStorage.setItem("todos", JSON.stringify(list));
  };

  const toggleComplete = (id) => {
    const newList = [...todos]
    const index = newList.findIndex((todo) => todo.id === id)
    const update = newList[index];
    newList.splice(index, 1, { ...update, complete: !update.complete });
    setTodos(newList)
    localStorage.setItem("todos", JSON.stringify(newList));
    
  }

  const onDelete = (id) => {
    let deleteTodos = todos.filter((todo) => todo.id !== id);
    setTodos(deleteTodos);
    localStorage.setItem("todos", JSON.stringify(deleteTodos));
  };

  return (
    <div className="App">
      <Form onAdd={onAdd} />
      <TaskList
        list={todos}
        setList={setTodos}
        onDelete={onDelete}
        toggleComplete={toggleComplete}
      />
    </div>
  );
}

export default App;
